package main

import "fmt"

// MyString : Unstructured type.
type MyString string

// Rect : Structured type with two variables.
type Rect struct {
	width  float64
	height float64
}

// Function with an empty interface as arg, it's purpose is to return the type and value of any "object" passed in arguments.
// Since all types implement an empty interface{} it's working for evry type passed to function.
func explain(i interface{}) {
	fmt.Printf("value give to explain function is of type '%T' with value %v\n", i, i)
}

func main() {
	ms := MyString("Fsck")
	r := Rect{5.5, 4.5}
	explain(ms)
	explain(r)
}
