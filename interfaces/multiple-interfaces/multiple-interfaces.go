package main

import (
	"fmt"
)

// Shape : Interface with Area func.
type Shape interface {
	Area() float64
}

// Object : Interface with Volume func.
type Object interface {
	Volume() float64
}

// Cube : Struct with side var.
type Cube struct {
	side float64
}

// Area : Functions with receptor Cube struct which calculates the area of a type struct Cube.
func (c Cube) Area() float64 {
	return 6 * c.side * c.side
}

// Volume : Same as Above but it generates the Volume value.
func (c Cube) Volume() float64 {
	return c.side * c.side * c.side
}

func main() {
	c := Cube{3}
	var s Shape = c
	var o Object = c
	fmt.Println("Area of the Cube is ", s.Area())
	fmt.Println("Volume  of the Cube is ", o.Volume())
	// We can't print o.Area or s.Volume because Shape does not define Volume and Object does not define Area.
	// In order to do that you'll need Type Assertion.
}
