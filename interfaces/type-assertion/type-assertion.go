package main

import (
	"fmt"
)

// Shape : Interface with Area func.
type Shape interface {
	Area() float64
}

// Object : Interface with Volume func.
type Object interface {
	Volume() float64
}

// Cube : Struct with side var.
type Cube struct {
	side float64
}

// Area : Functions with receptor Cube struct which calculates the area of a type struct Cube.
func (c Cube) Area() float64 {
	return 6 * c.side * c.side
}

// Volume : Same as Above but it generates the Volume value.
func (c Cube) Volume() float64 {
	return c.side * c.side * c.side
}

func main() {
	var s Shape = Cube{3}
	// In this exemple, we will find out the dynmic value of an interface using the syntax i.(Type) where i is the interface and Type is the targeted type.
	c := s.(Cube) // Extraction of the dynamic value of type Cube in c var. Now we can use Area and Volunme on c because it has the type Cube.
	// Trick to fail type assertion silently : value, ok := i.(Type)
	fmt.Println("Area of the Cube is ", c.Area())
	fmt.Println("Volume  of the Cube is ", c.Volume())
	// We can't print o.Area or s.Volume because Shape does not define Volume and Object does not define Area.
	// In order to do that you'll need Type Assertion.
}
