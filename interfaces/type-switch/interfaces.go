package main

import (
	"fmt"
	"strings"
)

// Functions explain takes an empty interface as argument, relative to the interface type it returns the right type.
// We extract the interface type using the assertion type i.(Interface).
func explain(i interface{}) {
	switch i.(type) {
	case string:
		fmt.Println("i stored string :", strings.ToUpper(i.(string)))
	case int:
		fmt.Println("i stroed int", i)
	default:
		fmt.Println("i stroed something else", i)
	}
}

func main() {
	explain("Hello World")
	explain(52)
	explain(true)
}
