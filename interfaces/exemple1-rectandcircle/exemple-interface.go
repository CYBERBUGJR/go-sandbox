package main

import (
	"fmt"
	"math"
)

// Shape : Interface with Area and Perimeter funcs.
type Shape interface {
	Area() float64
	Perimeter() float64
}

// Rect : Struct with two variables
type Rect struct {
	width  float64
	height float64
}

// Circle : Struct with radius var.
type Circle struct {
	radius float64
}

// Area : Functions with receptor Rect struct which calculates the area of a type struct Rect.
func (r Rect) Area() float64 {
	return r.width * r.height
}

// Perimeter : Same as Above but it generates the perimeter value.
func (r Rect) Perimeter() float64 {
	return 2 * (r.width + r.height)
}

// Area : Functions with receptor Circle struct which calculates the area of a type struct Rect.
func (c Circle) Area() float64 {
	return math.Pi * c.radius * c.radius
}

// Perimeter : Same as Above but it generates the perimeter value.
func (c Circle) Perimeter() float64 {
	return 2 * math.Pi * c.radius
}

func main() {
	var s Shape
	s = Rect{5.0, 4.0}
	r := Rect{5.0, 4.0}
	fmt.Printf("type of s is %T\n", s)
	fmt.Printf("value of s is %v\n", s)
	fmt.Println("Area of the rectangle is ", s.Area())
	fmt.Println("Does s == r ? Answer", s == r)
	// Polymorphysm of interface Shape s.
	s = Circle{10}
	fmt.Printf("type of s is %T\n", s)
	fmt.Printf("value of s is %v\n", s)
	fmt.Printf("value of s is %0.2f\n", s.Area())

}
