// Les méthodes sont des fonctions, on peut donc les écrire comme ceci :

package main

import (
	"fmt"
	"math"
)

// Vertex structured type vector
type Vertex2 struct {
	X, Y float64
}

// Abs Method to calculate absolute value of a vector.
func Abs(v Vertex2) float64 {
	return math.Sqrt(v.X*v.X + v.Y*v.Y)
}

func main() {
	v := Vertex2{3, 4}
	fmt.Println(Abs(v))
}
