package main

import (
	"fmt"
	"math"
)

// Vertex : Structured Type
type Vertex struct {
	X, Y float64
}

// Abs : Absolute valyue of a Vertex type.
func (v Vertex) Abs() float64 {
	return math.Sqrt(v.X*v.X + v.Y*v.Y)
}

// Scale : Function that returns multiplication of a vector by a scalar.
// The receptor must be a pointer because Scale needs to modify the value of v Vertex type.
// if pointer is ommited the value returned by the Scale func will be not saved.
func (v *Vertex) Scale(f float64) {
	v.X = v.X * f
	v.Y = v.Y * f
}

func main() {
	v := Vertex{3, 4}
	v.Scale(10)
	fmt.Println(v.Abs())
}
