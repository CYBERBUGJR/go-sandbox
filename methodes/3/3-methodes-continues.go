package main

import (
	"fmt"
	"math"
)

// MyFloat type non structuré.
type MyFloat float64

// Abs : Fonction sur un type non structuré.
func (f MyFloat) Abs() float64 {
	if f < 0 {
		return float64(-f)
	}
	return float64(f)
}

func main() {
	f := MyFloat(-math.Sqrt2)
	fmt.Println(f.Abs())
}
